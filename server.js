var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// http example data
var data = [{
    "userId" : "user001",
    "userName" : "순희",
    "userEmail" : "soon@mail.com"
}];

// app.use(express.bodyParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:9090");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
    res.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");
    next();
});

const port = 9090;
// static file use
// app.use('/static', express.static(__dirname + '/public'));
// public using folder
app.use(express.static(__dirname + '/public'));

//root
app.get('/', function (req, res) {
  // console.log(__dirname);
  // absolute path
  res.sendFile(__dirname + '/index.html');
});

// http example url start
app.get('/getUser', function(req, res){
    var userId = req.query.userId;
    var matched = {};

    data.forEach(function(data) {
        if(data.userId === userId) matched = data;
    })
    res.send(JSON.stringify(matched));
});

app.get('/getUsers', function(req, res){
    res.send(JSON.stringify(data));
});

app.get('/Ajax/find/', function(req, res){
    data.push(req.body);
    res.send("OK");
});

app.put('/updateUser', function(req, res){
    for (var i = data.length - 1; i >= 0; i--) {
        if(data[i].userId === req.body.userId) data[i] = req.body;
        break;
    };

    res.send("OK");
});

app.delete('/deleteUser', function(req, res){
    for (var i = data.length - 1; i >= 0; i--) {
        if(data[i].userId === req.query.userId) data.splice(i,1);
    };
    res.send("OK");
});

app.post('/insertUser', function (req, res)
{
   data.push(req.body);
   res.send("OK");
});
// http example url end

app.listen(port, function () {
  console.log('Example app listening on port '+port+' !');
});
