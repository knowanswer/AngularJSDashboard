/**
 * Created by owner on 2017-03-31.
 */
// homepage Controller
app.controller('homepageCtrl', ['$scope', '$route', function($scope, $route){
    $scope.route = $route;
    $scope.routes = $route.routes;
    $scope.$on("$routeChangeSuccess", function (e, cRoute, pRoute)
    {
        $scope.cPath = cRoute.loadedTemplateUrl;
        console.log("현재 라우트 정보 : ", cRoute.loadedTemplateUrl, cRoute);
        if(pRoute)
            console.log("이전 라우트 정보 : ", pRoute.loadedTemplateUrl);
    });
    $scope.reload = function ()
    {
        $route.reload();
    };
}]);

// '/' Main homeCtrl
app.controller('homeCtrl', function($scope, $rootScope){
    $rootScope.headTitle = "Main";
});

// '/todoList' todoListCtrl
app.controller('todoListCtrl', function($scope) {
    var todoList = this;
    todoList.todos = [
        {text:'learn AngularJS', done:true},
        {text:'build an AngularJS app', done:false}
    ];

    todoList.addTodo = function() {
        todoList.todos.push({text:todoList.todoText, done:false});
        todoList.todoText = '';
    };

    todoList.remaining = function() {
        var count = 0;
        angular.forEach(todoList.todos, function(todo) {
            count += todo.done ? 0 : 1;
        });
        return count;
    };

    todoList.archive = function() {
        var oldTodos = todoList.todos;
        todoList.todos = [];
        angular.forEach(oldTodos, function(todo) {
            if (!todo.done) todoList.todos.push(todo);
        });
    };
});

app.controller('selectCtrl', function ($scope) {
    var countryList = [
        {name:'한국', code:'KR', continent:'아시아'},
        {name:'일본', code:'JR', continent:'아시아'},
        {name:'미국', code:'EN', continent:'북미'}
    ];
    $scope.countryList = countryList;
});

// notice controller
app.controller('mainCtrl', function ($scope) {
    $scope.broadcast = function (noticeMsg) {
        $scope.$broadcast("chat:noticeMsg", noticeMsg);
        $scope.noticeMsg = "";
    };
});

app.controller('chatMsgListCtrl', function ($scope, $rootScope) {
    $scope.msgList = [];
    $rootScope.$on("chat:newMsg", function (e, newMsg) {
        $scope.msgList.push(newMsg);
    });

    $scope.$on("chat:noticeMsg", function (e, noticeMsg) {
        $scope.msgList.push("[공지] "+noticeMsg);
    });
});

app.controller('chatMsgInputCtrl', function ($scope) {
    $scope.submit = function (newMsg) {
        $scope.$emit("chat:newMsg", newMsg);
        $scope.newMsg = "";
    };
});

// bookmark controller
app.controller('bookmarkCtrl', ['$scope', function ($scope) {
    var bookmarkList = [
        {id:"google", name:"구글", url:"www.google.com"},
        {id:"naver", name:"네이버", url:"www.naver.com"},
        {id:"daum", name:"다음", url:"www.daum.com"}
    ];
    $scope.bookmarkList = bookmarkList;
}]);

// cookie controller
app.controller('cookieCtrl', ['$scope', '$cookieStore', function ($scope, $cookieStore) {
    $scope.value = $cookieStore.get("test") || "없음";
    $scope.getValue = function () {
        $scope.value = $cookieStore.get("test");
    };
    $scope.putValue = function (iV) {
        $cookieStore.put("test", iV);
    };
}]);

// sampleDirective controller
app.controller('sampleDirectiveCtrl', ['$scope', function ($scope) {
    $scope.getStyle = function () {
        return { "color":"red" };
    };
}]);

app.directive('hello', function($log){
   return function (scope, iElement, iAttrs, controller, trnscludeFn) {
       $log.log("<h1>hello " + iAttrs.name + "</h1>");
       iElement.html("<h1>hello " + iAttrs.name + "</h1>");
   };
});

// setScope controller, directive
app.controller('setScopeCtrl', [ '$scope', function ($scope) {
    $scope.name = "Ctrl에서 사용된 name 모델";
    $scope.helloList = [
        { name: 'google'},
        { name: 'naver'},
        { name: 'angular'}
    ];
    $scope.sendMessage = function (toSb) {
        console.log(toSb+"에게 메시지를 보낸다.");
    }
}]);
// directive name is camel case
app.directive('setScope', function () {
    return {
        templateUrl : "views/setScopeTmpl.html",
        restrict : "AE",
        //scope : true,\
        scope : {
            // abc : "@to",
            // send : "&"
            abc : "=to"
        }
    }
});

// directiveTransclude controller
app.controller('directiveTranscludeCtrl', ['$scope', function ($scope) {
    $scope.noticeList = [
        { url: "notice/1", text: "공지사항 첫 글" },
        { url: "notice/2", text: "공지사항 두 글" },
        { url: "notice/3", text: "공지사항 세 글" }
    ];
}]);
app.directive('panel', function () {
    return {
        templateUrl: "views/panelTmpl.html",
        restrict: "AE",
        transclude: true,
        scope: {
            title: "@",
            type: "@"
        }
    }
});

// sparkline controller
app.controller('sparklineCtrl', ['$scope', function ($scope)
{
    $scope.chartData = [0,0,0];
    $scope.selectedData = { x: null, y: null };
    $scope.handleClick = function (x, y)
    {
        $scope.selectedData.x = x;
        $scope.selectedData.y = y;
    }
}]);
app.directive('ngSparkline', [function ()
{
    return {
        restrict: 'EA',
        scope: {
            chartData: "=",
            chartClick: "&"
        },
        link: function (scope, iElement, iAttrs)
        {
            var options = { type: iAttrs.chartType || 'line' };
            scope.$watch('chartData', function(bfData, afData)
            {
                jQuery(iElement).sparkline(scope.chartData, options);
            }, true);

            jQuery(iElement).bind('sparklineClick', function(ev)
            {
               var sparkline = ev.sparklines[0];
               var region = sparkline.getCurrentRegionFields();
               var regionX = region.x;
               var regionY = region.y;

               if(regionX === undefined && region[0])
                   regionX = region[0].offset;
               if(regionY === undefined && region[0])
                   regionY = region[0].value;
               scope.$apply(function ()
               {
                  scope.chartClick({ x: regionX, y: regionY });
               });
            });
        }
    }
}]);

// helloService factory controller
app.factory('hello', [function ()
{
    var helloText = "님 안녕하세요.";

    return {
        say: function (name) {
            return name + helloText;
        }
    };
}]);
app.controller('helloServiceCtrl', ['$scope', 'hello', function ($scope, hello)
{
    $scope.hello = hello.say("철수");
}]);

// userFactory factory controller
app.factory('AppNm', [function ()
{
    return 'demo app';
}]);
app.factory('UserResource', [function ()
{
    var userList = [];

    return {
        addUser: function (newUser)
        {
            userList.push(newUser);
        },
        updateUser: function (idx, updatedUser)
        {
            userList[idx] = updatedUser;
        },
        deleteUser: function (idx)
        {
            userList[idx] = undefined;
        },
        selectUsers: function ()
        {
            return userList;
        }
    }
}]);
app.controller('userFactoryCtrl', ['$scope', 'AppNm', 'UserResource', function ($scope, AppNm, UserResource)
{
    $scope.appNm = AppNm;
    $scope.users = UserResource.selectUsers();

    $scope.addNewUser = function (newUser)
    {
        UserResource.addUser({
            name: newUser.name,
            email: newUser.email
        });
    };
}]);

// service example
function Calculator($log){
    this.lastValue = 0;

    this.add = function (a, b)
    {
        var returnV = a + b;
        $log.log(this.lastValue);
        this.lastValue = returnV;
        return returnV;
    };
    this.minus = function (a, b)
    {
        var returnV = a - b;
        $log.log(this.lastValue);
        this.lastValue = returnV;
        return returnV;
    };
}
app.factory('CalcByF', ['$log', function ($log)
{
    return new Calculator($log);
}]);
app.service('CalcByS', Calculator);
app.controller('calCtrl', ['$scope', '$log', 'CalcByS', 'CalcByF', function ($scope, $log, CalcByS, CalcByF) {
    $scope.val1 = CalcByF.add(10, 3);
    console.log(CalcByF.lastValue);
    $scope.val2 = CalcByS.minus(20, 10);
    console.log(CalcByS.lastValue);
}]);

// service provider example
app.provider('Logger', [function ()
{
    var defaultLogLevel = 'log';

    function Logger(msg)
    {
        if(checkNativeLogger)
        {
            if(defaultLogLevel === "debug")
            {
                console.debug(msg);
                return;
            }
            if(defaultLogLevel === "info")
            {
                console.info(msg);
                return;
            }
            console.log(msg);
        }
    }

    Logger.debug = function (msg)
    {
        if(checkNativeLogger)
            console.debug(msg);
    };

    Logger.info= function (msg)
    {
        if(checkNativeLogger)
            console.info(msg);
    };

    function checkNativeLogger()
    {
        if(console)
            return true;
        return false;
    }

    this.setDefaultLevel = function (level)
    {
        switch(level)
        {
            case 'debug':
                defaultLogLevel = 'debug';
                break;
            case 'info':
                defaultLogLevel = 'info';
                break;
            default:
                defaultLogLevel = 'log';
        }
    };

    this.$get = [function ()
    {
        return Logger;
    }];
}]);
app.config(['LoggerProvider', function (LoggerProvider) {
    LoggerProvider.setDefaultLevel("info");
}]);
app.controller('prov', ['$scope', 'Logger', function ($scope, Logger)
{
    Logger("console.log로 출력하는 로그메시지");
    Logger.debug("console.debug로 출력하는 로그메시지");
}]);
// constant
app.constant('PI', 3.141592);
app.provider('Cal', [function ()
{
    var defaultRaidus = 10;

    this.setDefaultRadius = function (radius)
    {
        defaultRadius = radius;
    }

    this.$get = ['PI', function (PI)
    {
        return {
            getCircleArea: function(radius)
            {
                var r = radius || defaultRaidus;
                return r*r*PI;
            }
        };
    }];
}]);

app.config(function(CalProvider, PI)
{
   CalProvider.setDefaultRadius(5);
   // console.log(PI);
});

app.directive('circle', ['Cal', 'PI', function (Cal, PI)
{
    return {
        restrict: 'E',
        template: '<canvas width="100" height="100"></canvas>',
        link: function(scope, iElement, iAttrs)
        {
            var context = iElement.find("canvas")[0].getContext('2d');
            var radius = 30;

            context.beginPath();
            context.arc(50, 50, radius, 0, 2*PI, false);
            context.fillStyle = 'green';
            context.fill();
            context.lineWidth = 5;
            context.stroke();
            iElement.append("<p>반지름 30px인 원의 넓이 : "+Cal.getCircleArea(radius)+"px </p>");
        }
    };
}]);

// injector
app.factory('Hello', [function ()
{
    return {
        helloTo: function (name)
        {
            return 'hello '+name;
        }
    };
}]);

// filter example
app.controller('filterCtrl', ['$scope', function ($scope)
{
    $scope.userList = [
        { userId: 'jay', userName:'제이', userEmail:'jay@ng.com' },
        { userId: 'soon', userName:'순희', userEmail:'soon@ng.com' },
        { userId: 'cindy', userName:'가영', userEmail:'cindy@ng.com' },
        { userId: 'mino', userName:'민호', userEmail:'mino@ng.com' },
        { userId: 'teapong', userName:'태풍', userEmail:'teapong@ng.com' },
    ];
    $scope.hello = "hello";
}]);

// custom filter
app.config(function ($filterProvider)
{
    $filterProvider.register('capitalize', function ()
    {
        return function (text)
        {
            return text.charAt(0).toUpperCase() + text.slice(1);
        };
    });
});

// contact
app.factory('ContactSvc', [function ()
{
    var cList = [
        { id: 'c001', name: '순희', email: 'soon@ng.com', phone: '011-2222-3333' },
        { id: 'c002', name: '철수', email: 'cheol@ng.com', phone: '011-1111-3333' }
    ];

    return {
        getList: function ()
        {
          return cList;
        },
        get: function (id)
        {
            var returnObj = {};
            for(var i = 0; i < cList.length; i++)
            {
                if(id === cList[i].id)
                {
                    returnObj = cList[i];
                    break;
                }
            }
            return returnObj;
        }
    };
}]);

app.controller('contactCtrl', ['$scope', '$location', 'ContactSvc', function ($scope, $location, ContactSvc)
{
    $scope.contactList = ContactSvc.getList();
    $scope.viewDetail = function(id)
    {
        $location.path('/Contacts/'+id);
    };
}]);
// app.controller('contactDetailCtrl', ['$scope', '$routeParams', 'ContactSvc', function ($scope, $routeParams, ContactSvc)
// {
//     $scope.contact = ContactSvc.get($routeParams.contactId);
// }]);

app.controller('contactDetailCtrl', ['$scope', 'contact', function ($scope, contact)
{
    $scope.contact = contact;
}]);

// http example
app.directive('infoBox', [function ()
{
    return {
        restrict: 'E',
        scope: { infoMessage: '=' },
        template: '<p class="info">{{ infoMessage }}<button ng-click="hide()">x</button></p>',
        link: function (scope, iElement, iAttrs)
        {
            scope.$watch("infoMessage", function (newData, beforeData)
            {
               if(newData === undefined || newData === ''){
                   scope.hide();
               }else{
                   iElement.show({
                       duration: 3000,
                       complete: function ()
                       {
                           iElement.hide();
                       }
                   });
               }
            });
            scope.hide = function ()
            {
                iElement.hide();
                scope.infoMessage = undefined;
            };
        }
    };
}]);
app.controller('httpCtrl', ['$scope', '$http', function ($scope, $http)
{
    $scope.user = {};
    $scope.search = function ()
    {
        var reqPromise = $http({
            method: 'GET',
            url: 'json/sample.json'
        });
        reqPromise.success(function (data, status, headers, config)
        {
            $scope.user = data;
        });
        reqPromise.then(function(response)
        {
            $scope.msg = response.data.userId + '로딩 완료.';
        },function (response)
        {
            $scope.msg = "Error!";
        });
    };
}]);
app.value('baseUrl', 'http://localhost:9090/');
app.controller('httpExamCtrl', ['$scope', '$http', 'baseUrl', function ($scope, $http, baseUrl)
{
    $scope.user = {};
    $scope.editable = false;
    $scope.search = function (searchObj)
    {
        var reqPromise = $http.get(baseUrl+'getUser',{ params:searchObj });
        reqPromise.success(function (data, status, headers, config)
        {
            if(data.userId)
            {
                $scope.user = data;
                $scope.isSearched = true;
            }
            $scope.error = undefined;
        });
        reqPromise.error(error);
    };

    $scope.insert = function ()
    {
        $scope.edit = "insert";
        $scope.editable = true;
        $scope.user = {};
    };

    $scope.update = function ()
    {
        $scope.edit = "update";
        $scope.editable = true;
    };

    $scope.save = function (edit, user)
    {
        var reqPromise;
        switch(edit)
        {
            case "insert":
                reqPromise = $http.post(baseUrl+'insertUser', user);
                break;
            case "update":
                reqPromise = $http.put(baseUrl+'updateUser', user);
                break;
            default:
                reqPromise = {};
                break;
        }
        reqPromise.success(function (data, status, headers, config)
        {
            console.log("save");
            reset();
        });
        reqPromise.error(error);
    };

    $scope.delete = function (user)
    {
        $http.delete(baseUrl+'deleteUser', { params: user }).success(function ()
        {
            reset();
        });
    };

    $scope.cancel = function ()
    {
        reset();
    };

    function error(data, status, headers, config)
    {
        $scope.user = {};
        $scope.error = "로드 실패";
    }

    function reset()
    {
        $scope.user = {};
        $scope.edit = undefined;
        $scope.error = undefined;
        $scope.editable = false;
        $scope.isSearched = false;
    }
}]);

// http data conversion
// app.config(function ($httpProvider)
// {
//     $httpProvider.defaults.headers.common['Accept'] = "application/xml";
//     $httpProvider.defaults.headers.post['Content-Type'] = "application/xml";
//     $httpProvider.defaults.transformResponse.push(function (data, getHeader)
//     {
//         if(getHeader("Content-Type") === "application/xml")
//         {
//             var x2js = new X2JS();
//             var obj = x2js.xml_str2json(data);
//             return obj;
//         }
//         else
//         {
//             return data;
//         }
//     });
// });
app.controller('xmlCtrl', ['$scope', '$http', function($scope, $http)
{
    $scope.header = $http.defaults.headers;
    $http({ method: 'GET', url: 'xml/test.xml' }).success(function(data){
        $scope.result = data;
    });
}]);

// restful example controller
// app.run(function ($resource,mogolabApiKey)
// {
//
//     var TodoResource = $resource('https://api.mlab.com/api/1/databases/sample/collections/todos/:todoId?' +
//         'apiKey=:apiKey',{ apiKey: mogolabApiKey}, {
//         'update': {
//             method: 'PUT'
//         }
//     });
//
//     var todo1 = new TodoResource;
//     todo1.done = false;
//     todo1.title = "공부하기";
//     todo1.$save(function ()
//     {
//         console.debug(todo1);
//         var id = todo1["_id"].$oid;
//         TodoResource.update({
//           todoId: id
//         }, JSON.stringify({done: true, title: todo1.title}), function ()
//         {
//             var updatedTodo1 = TodoResource.get({
//                 todoId: id
//             }, function ()
//             {
//                 if(updatedTodo1.done === true)
//                 {
//                     updatedTodo1.$delete({
//                         todoId: id
//                     });
//                 }
//             });
//         })
//     });
// });

// promise controller
app.controller('promiseCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
    var threeSecPromise = $timeout(function ()
    {
        return $scope.answer;
    }, 3000);
    threeSecPromise.then(function (val)
    {
        console.log(val);
        if(val == 39)
        {
            $scope.result="맞았어요.";
        }
        else
        {
            $scope.result="틀렸어요.";
        }
    }, function ()
    {
        $scope.result = "너무 어려웠나요?";
    });

    threeSecPromise.finally(function ()
    {
       $scope.info = "다시 시작하려면 refresh 해주세요.";
    });

    $scope.giveUp = function ()
    {
        $timeout.cancel(threeSecPromise);
    };
}]);

// deferred controller
app.factory('Teacher', [function ()
{
    function Teacher(name)
    {
        this.name = name;
    }
    Teacher.prototype.makeScore = function (data)
    {
        if(data > 5)
        {
            return 100;
        }
        else
        {
            return 4;
        }
    };
    Teacher.prototype.giveCandy = function (num, student)
    {
        return student.name + "(군/양) 사탕" + num + "개 받으세요.";
    };

    Teacher.prototype.hitHip = function (num, student, error)
    {
        return student.name + "(군/양) 엉덩이" + num + "대 맞으세요. "+ error;
    };

    return Teacher;
}]);
app.factory('Student', ['$q', '$timeout', function ($q, $timeout)
{
    function Student(name)
    {
        this.name = name;
    }
    Student.prototype.doHomework = function (homework)
    {
        var deferred = $q.defer();
        console.log(homework.length);
        var time = (Math.random() * 10 + homework.length)*500;
        console.log("숙제하는데 걸리는 시간 : ",time);
        $timeout(function ()
        {
            var homeworkResult = time/1000;
            console.log("숙제 결과 : ", homeworkResult);
            if(time < 6000)
            {
                deferred.resolve(homeworkResult);
            }
            else
            {
                deferred.reject("핑계들...");
            }
        }, time);

        return deferred.promise;
    }
    return Student;
}]);

app.controller('deferredCtrl', ['$scope', '$timeout', 'Teacher', 'Student', function ($scope, $timeout, Teacher, Student)
{
    var jay = new Student('jay');
    var cindy = new Teacher('cindy');
    var promiseWithStudent = jay.doHomework('숙제내용...');

    promiseWithStudent.then(function (data)
    {
        if(cindy.makeScore(data) === 100)
        {
            console.log(cindy.giveCandy(100,jay));
        }
        else
        {
            console.log(cindy.giveCandy(50,jay));
        }
    }, function (error)
    {
        console.log("여기");
        console.log(cindy.hitHip(1000000, jay, error));
    });
}]);

// qall controller
app.controller('qallCtrl', ['$scope', '$q', '$http', '$timeout', function ($scope, $q, $http, $timeout)
{
    $scope.userList = [];
    var httpPromise1 = $timeout(function ()
    {
        console.log("load sample...");
        return $http.get('json/sample.json');
    }, 5000);
    var httpPromise2 = $timeout(function ()
    {
        console.log("load sample2...");
        return $http.get('json/sample2.json');
    }, 3000);

    $q.all([httpPromise1, httpPromise2])
        .then(function (resultArray)
        {
            angular.forEach(resultArray, function (value, key)
            {
                $scope.userList.push(value.data);
            });
        });
}]);

// app.config(function ($httpProvider)
// {
//     $httpProvider.interceptors.push(function ($q, $timeout)
//     {
//         return {
//             'request': function (config)
//             {
//                 return $timeout(function ()
//                 {
//                     return config;
//                 }, 3000);
//             },
//             'response': function (response)
//             {
//                 return response || $q.when(response);
//             }
//         };
//     });
// });
app.controller('interceptCtrl', ['$scope', '$http', function ($scope, $http)
{
    $scope.send = function ()
    {
        $http({ method: 'GET', url: 'json/sample.json' })
            .success(function (data)
            {
                $scope.result = data;
            });
    };
}]);