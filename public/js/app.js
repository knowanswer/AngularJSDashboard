/**
 * Created by owner on 2017-03-30.
 */
var app = angular.module("dashboard", ["ngRoute", "ngCookies", "ngResource"]);
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "views/dashboard.htm",
            controller : "homeCtrl"
        })
        .when("/TodoList", {
            templateUrl : "views/todoList.htm",
            controller : "todoListCtrl"
        })
        .when("/Overview", {
            templateUrl : "views/overview.htm"
        })
        .when("/Select", {
            templateUrl : "views/select.htm",
            controller : "selectCtrl"
        })
        .when("/Notice", {
            templateUrl : "views/notice.htm"
        })
        .when("/Bookmark", {
            templateUrl : "views/bookmark.htm"
        })
        .when("/Cookie", {
            templateUrl : "views/cookie.htm"
        })
        .when("/SampleDirective", {
            templateUrl : "views/sampleDirective.htm"
        })
        .when("/SetScope", {
            templateUrl : "views/setScope.htm"
        })
        .when("/DirectiveTransclude", {
            templateUrl : "views/directiveTransclude.htm"
        })
        .when("/Sparkline", {
            templateUrl : "views/sparkline.htm"
        })
        .when("/HelloService", {
            templateUrl : "views/helloService.htm"
        })
        .when("/UserFactory", {
            templateUrl : "views/userFactory.htm"
        })
        .when("/Injector", {
            templateUrl : "views/injector.htm"
        })
        .when("/Filter", {
            templateUrl : "views/filter.htm"
        })
        .when("/Contacts", {
            templateUrl : "views/contactList.htm"
        })
        .when("/Contacts/:contactId", {
            templateUrl : "views/contactDetail.htm",
            controller: 'contactDetailCtrl',
            resolve: {
                contact: function ($q, $route, $timeout, ContactSvc)
                {
                    var deferred = $q.defer();
                    $timeout(function ()
                    {
                        var result = ContactSvc.get($route.current.params.contactId);
                        deferred.resolve(result);
                    }, 2000);
                    return deferred.promise;
                }
            }
        })
        .when("/Http", {
            templateUrl : "views/http.htm"
        })
        .when("/Promise", {
            templateUrl : "views/promise.htm"
        })
        .when("/Deferred", {
            templateUrl : "views/deferred.htm"
        })
        .when("/Qall", {
            templateUrl : "views/qall.htm"
        })
        .otherwise({
            redirectTo : "/"
        });
    // use html5 to get rid of # in url
    // $locationProvider.html5Mode(true);
}]);

var menu = [
    "Qall",
    "Deferred",
    "Promise",
    "Http",
    "Contacts",
    "Filter",
    "Injector",
    "UserFactory",
    "HelloService",
    "Sparkline",
    "DirectiveTransclude",
    "SetScope",
    "SampleDirective",
    "Cookie",
    "Bookmark",
    "Notice",
    "Select",
    "Overview",
    "TodoList"
];
